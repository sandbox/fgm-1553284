<?php
/**
 * @file
 * Trivial logger for easier debug.
 */

namespace OSInet\Class_Grapher;

class Logger {
  var $debugLevel;

  public function __construct($debugLevel) {
    $this->debugLevel = $debugLevel;
  }

  /**
   * Output a message on STDERR if its relevance is above minimum level.
   *
   * @param string $message
   * @param int $level
   *   Defined in RFC 3164, section 4.1.1 "Severity". But compare with Drupal 7
   *   WATCHDOG_* constants in includes/bootstrap.inc for an explanation about
   *   syslog constants in PHP.
   */
  public function debug($message = "\n", $level = LOG_INFO) {
    if ($level <= $this->debugLevel) {
      fputs(STDERR, $message);
      fflush(STDERR);
    }
  }
}
