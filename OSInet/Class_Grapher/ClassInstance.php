<?php

namespace OSInet\Class_Grapher;

/**
 * Representation of a "class" symbol in source code.
 */
class ClassInstance extends InterfaceInstance {
  public $interfaces = array();

  /**
   * @param \PGPClass $symbol
   *   The symbol returned by grammar_parser.
   * @param int $implicit
   * @param Logger $logger
   */
  public function __construct(\PGPClass $symbol, $implicit = 0, Logger $logger) {
    parent::__construct($symbol, $implicit, $logger);
    $this->interfaces = $symbol->implements;
  }

  public function basicAttributes() {
    $ret = array(
      'shape' => 'rect',
    );
    return $ret;
  }
}
