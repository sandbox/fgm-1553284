<?php
/**
 * This code uses several external classes:
 *
 * - PSR0-compliant
 *   - OSInet Class_Grapher package.
 * - PEAR
 *   - Image_GraphViz
 * - Miscellaneous
 *   - grammar_parser package 7.x -
 *     - http://drupal.org/project/grammar_parser
 *     - does not depend on Drupal, but can be used within Drupal/Drush either
 *       as a module or a Libraries-compatible PHP library.
 *
 * It also depends on
 * - GraphViz - http://www.graphviz.org/
 * - PHP 5.3
 */
// To use, adjust the grammar parser path to your deployment.
$gpPath = '../../d7/sites/all/libraries/grammar_parser/engine';

use \OSInet\Class_Grapher\Logger;
use \OSInet\Class_Grapher\Graph;

// Image_GraphViz throws notices, so keep it quiet.
error_reporting(E_WARNING);

// Include the OSInet PSR0 autoloader.
require __DIR__ .'/../../misc/psr0.php';

// Load Image_GraphViz: PEAR, not PSR0.
require 'Image/GraphViz.php';

// Load grammar_parser. Not autoloadable.
$gpFiles = array('parser', 'editor', 'list', 'object', 'reader', 'writer');
foreach ($gpFiles as $file) {
  require "$gpPath/$file.inc";
}

// Initialize PSR0 autoloader for remaining classes.
spl_autoload_register('psr0_autoload', TRUE);

// Suggestion: look at Drupal 8 core/lib/Drupal/Core/Database directory
$base = $argv[1];

$logger = new Logger(LOG_DEBUG);
$imager = 'dot';
$format = 'svg';

$graph = new Graph($base, $logger);
echo $graph->build($imager, $format);

