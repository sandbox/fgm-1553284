<?php
/**
 * Sample PSR-0 autoloader.
 *
 * Do not use as such: it is only present to demo classes in a PSR-0 context.
 *
 * Straight from the PSR-0 standard.
 *
 * @link https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md
 *
 * @param string $className
 */
function psr0_autoload($className) {
  $className = ltrim($className, '\\');
  $fileName  = '';
  $namespace = '';
  if ($lastNsPos = strripos($className, '\\')) {
    $namespace = substr($className, 0, $lastNsPos);
    $className = substr($className, $lastNsPos + 1);
    $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
  }
  $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
  /*
   $stack = array_slice(debug_backtrace(), 2, 1);
  $stack = reset($stack);
  unset($stack['args']);
  print_r($stack);
  */
  $sts = @include $fileName;
  return $sts;
}
